const { Client, Intents, } = require('discord.js');
const client = new Client({ intents: [Intents.FLAGS.GUILDS, Intents.FLAGS.GUILD_MESSAGES] });
const config = require('./config.json')

// parsing some config items
const messageTimeInterval = parseInt(config.messageTimeInterval, 10)
const messageInterval = parseInt(config.messageInterval, 10)

// watchers
let lastSentMessage = new Date().getTime() - messageTimeInterval
let nbOfMessage = 0


/**
 * @param {string} string
 * @returns {boolean}
 */
function hasCRX(string) {
    // if exact match
    if(string.includes('CRX_HEADER_INVALID')) return true
    // check if the error message is not necessarily copy pasted.
    const lowerCaseString = string.toLocaleLowerCase()
    if(lowerCaseString.includes('crx') && lowerCaseString.includes('invalid') && lowerCaseString.includes('header')) {
        return true
    }
    return false
}

/**
 * @param {object} channel
 * @param {string} message
*/
function sendMessage(channel, message) {
    // ignore anti-flood if part of role excludeRoles
    if(!message.member.roles.cache.some(role => config.excludeRoles.includes(role.name.toLocaleLowerCase()))) {
        channel.send(message)
        return
    }

    const compare = new Date().getTime() - lastSentMessage
    // if a certain amount of time passed && at least 10 message (from other users) have been sent before
    if(compare > messageTimeInterval && nbOfMessage % messageInterval === 0) {
        // reset watchers
        lastSentMessage = new Date().getTime()
        nbOfMessage = 0
        // send message
        channel.send(message)
    }
}

client.on('ready', () => {
  console.log(`Logged in as ${client.user.tag}!`);
    client.on('messageCreate', message => {
        if(client.user.id === message.author.id) return
        if(hasCRX(message.content)) {
            // send message only if user role isn't part of excludeRoles
                sendMessage(message.channel, `:robot: Beep.. Bop: <@${message.author.id}> CRX_HEADER_INVALID: follow install instructions at <${config.installAMRlink}>`)
        } else if (message.content.startsWith(config.prefix + "crx")) {
            sendMessage(message.channel, `Beep.. Bop :robot: CRX_HEADER_INVALID: follow install instructions at <${config.installAMRlink}>`);
        }
        if(nbOfMessage === 10) return 
        nbOfMessage++
    })
});
client.login(config.token);
