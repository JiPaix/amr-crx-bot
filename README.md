# AMR CRX BOT
This bot autmatically answer users that send a message containing the words crx invalid header, whatever the order as long as these three words are in the same sentence.

## how to configure:
```json
{
    "prefix": "!",
    "installAMRlink": "https://gitlab.com/all-mangas-reader/all-mangas-reader-2#install-amr-on-a-chromium-browser",
    "messageInterval": "10",
    "messageTimeInterval": "600000",
    "excludeRoles": ["developers", "admin", "beta_builds"],
    "token": ""
}
```
- **prefix**: the prefix to use when calling the crx command, default: `!crx`
- **installAMRlink**: link to the installation instructions
- **messageInterval**: The minimal amount of message to be sent before the bot is allowed to talk again (also applies to !crx)
- **messageTimeInterval**: The minimal amount of time between two message sent by the bot (also applies to !crx)
- **excludeRoles**: array of roles (in lower case) which aren't affected by `messageInterval` and `messageTimeInterval`
- **token**: discord.js API token

## install and start
```bash
npm install
npm start
```
